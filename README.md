# Meltano Demo Project Guide

## Initial Setup and Pre-requisites

Python 3.7+
Docker (Optional)

### With Pip

```bash
mkdir meltano-projects
cd meltano-projects

python3 -m venv .venv
source .venv/bin/activate

pip3 install meltano

meltano init my-meltano-project
cd my-meltano-project/
```

#### With Docker

```bash
docker pull meltano/meltano:latest
docker run meltano/meltano --version

mkdir meltano-projects
cd meltano-projects

docker run -v $(pwd):/project -w /project meltano/meltano init my-meltano-project
cd my-meltano-project/
```

***

## Starting up

If the project is already setup run:

```bash
### With Pipenv
pipenv install
pipenv shell
meltano install
```

### Meltano's UI

```bash
meltano ui (--bind-port xxxx)
```

### Start Airflow orchestrator

```bash
meltano invoke airflow users create \
-e suleiman@zuhlke.com \
-f Suleiman \
-l Deni \
-p pass \
-r Admin \
-u sulydeni
```

```bash
meltano invoke airflow scheduler
meltano invoke airflow webserver
```

### Configure and start Docker

```bash
meltano add files docker
meltano add files docker-compose

##Docker compose dev
docker-compose up

##Docker compose production with the postgres DB
docker-compose -f docker-compose.prod.yml up
```
For more info on Docker Compose [click here](README%20(docker-compose).md). 
***

### Useful Links

- [Meltano Hub](https://docs.meltano.com/)
- [Meltano Docs](https://docs.meltano.com/)
- Slack Community: https://meltano.com/slack/

### Key Posts

- [Modern Data Stack Keynote](https://youtu.be/AqrTojIYjac?t=99)
- [Deploying Meltano](https://meltano.com/blog/deploying-meltano-for-meltano/)
- [Modern Data integrations tools roundup
](https://meltano.com/blog/modern-data-integrations-tools-roundup/)
